import device;
import AudioManager;
import ui.TextView as TextView;
import ui.ImageView as ImageView;
import ui.View as View;
import src.Config as config;
import src.MainMenuScreen as MainMenuScreen;
import src.GameScreen as GameScreen;
import src.LevelScreen as LevelScreen;
import ui.StackView as StackView;

var BG_WIDTH = config.bgWidth;
var BG_HEIGHT = config.bgHeight;
var app;
exports = Class(GC.Application, function () {

  this._settings = {
    alwaysRepaint: true,
    preload: ["resources/images"]
  };

  this.initUI = function () {

    app = this;

    this.setScreenDimensions(BG_WIDTH > BG_HEIGHT);

    GLOBAL.screenWidth = config.bgWidth;
    GLOBAL.screenHeight = config.bgHeight;

    GLOBAL.curStage = 1;
    GLOBAL.curLv = -1;

    var mainMenuScreen = new MainMenuScreen();
    var gameScreen = new GameScreen();
    var levelScreen = new LevelScreen();

    this._sound = new AudioManager({
      path: "resources/sounds/",
      files: {
        click: {
          volume: 1,
          loop: false
        }
      }
    });

    var rootView = new StackView({
      superview: this.view,
      x: 0,
      y: 0,
      width: BG_WIDTH,
      height: BG_HEIGHT,
      scale: 1,
      clip: true,
      zIndex: 1,
      backgroundColor: "white"
    });

    rootView.push(mainMenuScreen);
    // rootView.push(levelScreen);

    //get player save
    if (NATIVE.localStorage) {
      GLOBAL.curStage = parseInt(NATIVE.localStorage.getItem("puralax_stage"));
      GLOBAL.curLv = parseInt(NATIVE.localStorage.getItem("puralax_level"));
    }else{
      GLOBAL.curStage = parseInt(localStorage.getItem("puralax_stage"));
      GLOBAL.curLv = parseInt(localStorage.getItem("puralax_level"));
    }

    if(isNaN(GLOBAL.curStage)) GLOBAL.curStage = 1;
    console.log("hungnt current stage: " + GLOBAL.curStage);


    if(isNaN(GLOBAL.curLv)) GLOBAL.curLv = -1;
    console.log("hungnt current level: " + GLOBAL.curLv);

    /* Listen for an event dispatched by the title screen when
     * the start button has been pressed
     */
    mainMenuScreen.on('MainMenuScreen:stage1', function(){
      levelScreen.generateLevel(1);
      app._sound.play("click");
      rootView.push(levelScreen);
    });

    mainMenuScreen.on('MainMenuScreen:stage2', function(){
      levelScreen.generateLevel(2);
      app._sound.play("click");
      rootView.push(levelScreen);
    });

    mainMenuScreen.on('MainMenuScreen:stage3', function(){
      levelScreen.generateLevel(3);
      app._sound.play("click");
      rootView.push(levelScreen);
    });

    gameScreen.on('GameScreen:back', function (stage) {
      levelScreen.generateLevel(stage);
      rootView.pop();
    });

    levelScreen.on('LevelScreen:back', function () {
      rootView.pop();
    });

    levelScreen.on('LevelScreen:level', function (levelNumber, stage) {
      console.log("hungnt game play " + levelNumber);
      rootView.push(gameScreen);
      gameScreen.startGame(levelNumber - 1, stage); //game start from level 0
    });

  };

  this.launchUI = function () {

  };

  /**
   * setScreenDimensions
   * ~ normalizes the game's root view to fit any device screen
   */
  this.setScreenDimensions = function(horz) {
    var ds = device.screen;
    var vs = this.view.style;
    vs.width = horz ? ds.width * (BG_HEIGHT / ds.height) : BG_WIDTH;
    vs.height = horz ? BG_HEIGHT : ds.height * (BG_WIDTH / ds.width);
    vs.scale = horz ? ds.height / BG_HEIGHT : ds.width / BG_WIDTH;
  };

});