var WHITE = "#f5f5f5";
var RED = "#d1536b";
var GREEN = "#a8d166";
var PURPLE = "#906AA8";
var YELLOW = "#EBC658";
var BLUE = "#4FB5D6";

exports = {
	color:{
		white: WHITE,
		red: RED,
		green: GREEN
	},
	stage1: [
		//level 1
		{
			target: RED, // level's target
			width:  3, // board width
			height: 3, // board height
			board:[
				[WHITE, 0], [WHITE, 0], [WHITE, 0], // [cell's color, steps]
				[WHITE, 0], [RED,  	1], [GREEN , 0],
				[WHITE, 0], [WHITE, 0], [WHITE, 0]
			]
		},
		//level 2
		{
			target: RED,
			width: 3,
			height: 3,
			board:[
				[WHITE, 0], [WHITE, 0], [WHITE, 0],
				[RED,   2], [WHITE, 0], [GREEN, 0],
				[WHITE, 0], [WHITE, 0], [WHITE, 0]
			]
		},
		//level 3
		{
			target: YELLOW,
			width: 3,
			height: 3,
			board:[
				[WHITE, 0], [WHITE, 0], [PURPLE, 0],
				[YELLOW,2], [WHITE, 0], [PURPLE, 0],
				[WHITE, 0], [WHITE, 0], [PURPLE, 0]
			]
		},
		//level 4
		{
			target: RED,
			width: 3,
			height: 3,
			board:[
				[WHITE, 0], [WHITE, 0], [WHITE, 0],
				[RED,	3], [WHITE, 0], [YELLOW, 0],
				[WHITE, 0], [GREEN, 0], [WHITE, 0]
			]
		},
		//level 5
		{
			target: GREEN,
			width: 3,
			height: 3,
			board:[
				[WHITE, 0], [YELLOW, 0], [YELLOW, 0],
				[GREEN,	3], [WHITE,  0], [PURPLE, 0],
				[WHITE, 0], [WHITE,  0], [PURPLE, 0]
			]
		},
		//level 6
		{
			target: YELLOW,
			width: 3,
			height: 3,
			board:[
				[WHITE, 0], [WHITE,  0], [WHITE,  0],
				[RED,	1], [WHITE,  0], [YELLOW, 1],
				[WHITE, 0], [GREEN,  1], [WHITE,  0]
			]
		},
		//level 7
		{
			target: RED,
			width: 3,
			height: 3,
			board:[
				[WHITE, 0], [GREEN,  0], [GREEN,  1],
				[RED,	2], [WHITE,  0], [YELLOW, 0],
				[WHITE, 0], [WHITE,  0], [YELLOW, 0]
			]
		},
		//level 8
		{
			target: GREEN,
			width: 4,
			height: 4,
			board:[
				[PURPLE, 0], [PURPLE,  0], [GREEN,  0], [GREEN,  1],
				[RED,	 2], [WHITE,   0], [WHITE,  0], [RED,	 0],
				[RED,	 0], [WHITE,   0], [WHITE,  0], [RED,	 1],
				[YELLOW, 0], [YELLOW,  0], [BLUE,   1], [BLUE,	 0]
			]
		},
		//level 9
		{
			target: PURPLE,
			width: 4,
			height: 4,
			board:[
				[PURPLE, 0], [PURPLE,  1], [GREEN,  0], [GREEN,  1],
				[RED,	 0], [WHITE,   0], [WHITE,  0], [RED,	 0],
				[RED,	 0], [WHITE,   0], [WHITE,  0], [RED,	 2],
				[YELLOW, 1], [YELLOW,  1], [BLUE,   0], [WHITE,	 0]
			]
		},
		//level 10
		{
			target: BLUE,
			width: 4,
			height: 4,
			board:[
				[RED, 	 0], [RED,		0], [GREEN,  0], [GREEN,  0],
				[RED,	 0], [BLUE,		0], [BLUE,	 1], [GREEN,  0],
				[WHITE,	 0], [YELLOW,	0], [YELLOW, 0], [WHITE,  0],
				[WHITE,  0], [YELLOW,	3], [YELLOW, 3], [WHITE,  0]
			]
		},
		//level 11
		{
			target: GREEN,
			width: 4,
			height: 4,
			board:[
				[BLUE, 	 0], [YELLOW,	0], [WHITE,  0], [YELLOW,  0],
				[RED,	 0], [YELLOW,	0], [GREEN,	 2], [YELLOW,  0],
				[RED,	 0], [WHITE,	0], [GREEN,  2], [WHITE,   0],
				[BLUE,   1], [WHITE,	0], [WHITE,	 0], [WHITE,   0]
			]
		},
		//level 12
		{
			target: YELLOW,
			width: 5,
			height: 5,
			board:[
				[BLUE, 	 0], [BLUE,		2], [WHITE,  0], [PURPLE,  0], [PURPLE,  0],
				[BLUE,	 0], [WHITE,	0], [WHITE,	 0], [WHITE,   0], [PURPLE,  2],
				[WHITE,	 0], [WHITE,	0], [YELLOW, 2], [WHITE,   0], [WHITE,   0],
				[RED,    2], [WHITE,	0], [WHITE,	 0], [WHITE,   0], [GREEN,   0],
				[RED,    0], [RED,		0], [WHITE,	 0], [GREEN,   2], [GREEN,   0]
			]
		},
		//level 13
		{
			target: GREEN,
			width: 5,
			height: 5,
			board:[
				[PURPLE, 0], [YELLOW,	1], [WHITE,  0], [YELLOW,  1], [PURPLE,  0],
				[YELLOW, 1], [WHITE,	0], [RED,	 1], [WHITE,   0], [YELLOW,  1],
				[WHITE,	 0], [RED,		1], [GREEN,  3], [RED,     1], [WHITE,   0],
				[YELLOW, 1], [WHITE,	0], [RED,	 1], [WHITE,   0], [YELLOW,  1],
				[PURPLE, 0], [YELLOW,	1], [WHITE,  0], [YELLOW,  1], [PURPLE,  0]
			]
		},
		//level 14
		{
			target: PURPLE,
			width: 5,
			height: 5,
			board:[
				[RED, 	 0], [GREEN,	1], [WHITE,  0], [GREEN,   1], [RED,	 0],
				[GREEN,  1], [WHITE,	0], [YELLOW, 1], [WHITE,   0], [GREEN,   1],
				[WHITE,	 0], [YELLOW,	1], [PURPLE, 1], [YELLOW,  1], [WHITE,   0],
				[GREEN,  1], [WHITE,	0], [PURPLE, 0], [WHITE,   0], [GREEN,   1],
				[RED, 	 0], [GREEN,	1], [WHITE,  0], [GREEN,   2], [RED,	 0]
			]
		},
		//level 15
		{
			target: PURPLE,
			width: 6,
			height: 8,
			board:[
				[YELLOW, 0], [YELLOW,	0], [YELLOW, 0], [YELLOW,  0], [WHITE,	 0], [WHITE,	0],
				[WHITE,  0], [WHITE,	0], [YELLOW, 1], [WHITE,   0], [WHITE,   0], [GREEN,	4],
				[WHITE,  0], [YELLOW,	0], [YELLOW, 0], [WHITE,   0], [WHITE,   0], [WHITE,	0],
				[YELLOW, 0], [YELLOW,	0], [WHITE,  0], [WHITE,   0], [WHITE,   0], [BLUE,		3],
				[YELLOW, 0], [WHITE,	0], [WHITE,  0], [WHITE,   0], [WHITE,   0], [WHITE,	0],
				[YELLOW, 0], [WHITE,	0], [WHITE,  0], [WHITE,   0], [WHITE,   0], [GREEN,	4],
				[YELLOW, 0], [YELLOW,	0], [GREEN,  0], [WHITE,   0], [WHITE,   0], [WHITE,	0],
				[PURPLE, 1], [WHITE,	0], [WHITE,  0], [RED,     3], [RED,     3], [WHITE,	0]
			]
		},
		//level 16
		{
			target: BLUE,
			width: 4,
			height: 4,
			board:[
				[RED, 	 0], [RED,		1], [WHITE,  0], [RED,     0],
				[WHITE,  0], [GREEN,	1], [WHITE,  0], [GREEN,   0],
				[WHITE,  0], [GREEN,	0], [WHITE,  0], [GREEN,   0],
				[BLUE, 	 1], [GREEN,	0], [BLUE,   3], [GREEN,   0]
			]
		},
		//level 17
		{
			target: BLUE,
			width: 5,
			height: 5,
			board:[
				[RED, 	 0], [RED,		2], [WHITE,  0], [GREEN,   0], [GREEN,   0],
				[RED,	 0], [WHITE,	0], [WHITE,  0], [WHITE,   0], [GREEN,   2],
				[WHITE,  0], [WHITE,	0], [PURPLE, 1], [WHITE,   0], [WHITE,   0],
				[YELLOW, 2], [WHITE,	0], [WHITE,  0], [WHITE,   0], [BLUE,    0],
				[YELLOW, 0], [YELLOW,	0], [WHITE,  0], [BLUE,    2], [BLUE,    0]
			]
		},
		//level 18
		{
			target: GREEN,
			width: 5,
			height: 5,
			board:[
				[YELLOW, 2], [YELLOW,	1], [YELLOW,  1], [YELLOW,   1], [YELLOW,   1],
				[WHITE,	 0], [WHITE,	0], [WHITE,	  0], [WHITE,    0], [WHITE,    0],
				[GREEN,  0], [GREEN,	0], [GREEN,	  0], [GREEN,    0], [GREEN,    2],
				[WHITE,	 0], [WHITE,	0], [WHITE,	  0], [WHITE,    0], [WHITE,    0],
				[RED,	 2], [RED,		1], [RED,  	  1], [RED,      1], [RED,    	1]
			]
		},
		//level 19
		{
			target: BLUE,
			width: 5,
			height: 5,
			board:[
				[GREEN, 0], [YELLOW,	1], [YELLOW,  2], [WHITE,	0], [GREEN,		0],
				[WHITE,	0], [WHITE,		0], [WHITE,	  0], [WHITE,   0], [GREEN,    	0],
				[RED,  	3], [PURPLE,	2], [GREEN,	  0], [YELLOW,  0], [GREEN,    	1],
				[WHITE,	0], [WHITE,		0], [WHITE,	  0], [WHITE,   0], [GREEN,    	0],
				[BLUE,	0], [BLUE,		0], [BLUE, 	  2], [WHITE,   0], [GREEN,    	0]
			]
		},
		//level 20
		{
			target: PURPLE,
			width: 4,
			height: 7,
			board:[
				[BLUE, 		0], [RED,		0], [RED,  		1], [WHITE,		0],
				[WHITE,		0], [GREEN,		3], [RED,	  	3], [YELLOW,    1],
				[YELLOW,  	0], [GREEN,		0], [PURPLE,	0], [WHITE,  	0],
				[YELLOW,  	0], [PURPLE,	0], [PURPLE,	1], [BLUE,  	1],
				[YELLOW,  	0], [GREEN,		0], [PURPLE,	0], [WHITE,  	0],
				[WHITE,  	0], [GREEN,		4], [RED,		0], [YELLOW,  	1],
				[BLUE,  	0], [RED,		0], [RED,		1], [WHITE,  	0]
			]
		}
	],
	stage2: [
		//level 1
		{
			target: BLUE, // level's target
			width:  4, // board width
			height: 4, // board height
			board:[
				[PURPLE, 0], [PURPLE, 1], [GREEN, 0], [BLUE, 0],// [cell's color, steps]
				[YELLOW, 0], [YELLOW, 0], [GREEN, 0], [BLUE, 0],
				[RED, 	 0], [RED, 	  0], [YELLOW, 1], [YELLOW, 1],
				[BLUE, 	 1], [RED, 	  0], [PURPLE, 1], [PURPLE, 0]
			]
		},
		//level 2
		{
			target: YELLOW,
			width: 4,
			height: 4,
			board:[
				[YELLOW, 0], [PURPLE, 0], [PURPLE, 	0], [BLUE, 		1],// [cell's color, steps]
				[PURPLE, 0], [PURPLE, 0], [GREEN, 	1], [GREEN, 	0],
				[RED, 	 0], [RED, 	  1], [YELLOW, 	1], [YELLOW, 	0],
				[GREEN,  0], [RED, 	  0], [RED, 	0], [BLUE, 		0]
			]
		},
		//level 3
		{
			target: PURPLE,
			width: 5,
			height: 6,
			board:[
				[GREEN, 	1], [RED, 		0], [PURPLE, 	1], [RED, 		1], [GREEN, 	0],// [cell's color, steps]
				[GREEN, 	0], [GREEN, 	0], [BLUE, 		0], [GREEN, 	0],	[GREEN, 	0],
				[RED, 	 	0], [YELLOW, 	0], [GREEN, 	0], [YELLOW, 	1], [RED, 		0],
				[RED, 	 	0], [YELLOW, 	0], [GREEN, 	0], [YELLOW, 	0], [RED, 		0],
				[GREEN, 	0], [GREEN, 	0], [BLUE, 		2], [GREEN, 	0],	[GREEN, 	0],
				[GREEN, 	1], [RED,		0], [PURPLE, 	0], [RED, 		1], [GREEN, 	0]
			]
		},
		//level 4
		{
			target: BLUE,
			width: 5,
			height: 7,
			board:[
				[GREEN, 	1], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [GREEN, 	0],// [cell's color, steps]
				[GREEN, 	0], [RED, 		0], [PURPLE, 	0], [RED, 		0],	[GREEN, 	0],
				[GREEN, 	0], [RED, 		0], [PURPLE, 	1], [RED, 		0],	[GREEN, 	0],
				[YELLOW, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		1], [GREEN, 	0],
				[YELLOW, 	0], [RED, 		1], [PURPLE, 	0], [RED, 		0],	[YELLOW, 	1],
				[YELLOW, 	0], [RED, 		0], [PURPLE, 	0], [RED, 		0],	[YELLOW, 	0],
				[YELLOW, 	0], [GREEN, 	1], [GREEN, 	0], [GREEN, 	0],	[YELLOW, 	0]
			]
		},
		//level 5
		{
			target: RED,
			width: 5,
			height: 7,
			board:[
				[PURPLE, 	0], [RED, 		0], [PURPLE, 	0], [YELLOW, 	0], [YELLOW, 	1],// [cell's color, steps]
				[GREEN, 	0], [RED, 		1], [PURPLE, 	1], [YELLOW, 	0],	[PURPLE, 	0],
				[BLUE, 		0], [GREEN, 	0], [YELLOW, 	0], [RED, 		0],	[RED, 		0],
				[BLUE, 		0], [BLUE, 		1], [YELLOW, 	1], [GREEN, 	1], [GREEN, 	0],
				[RED, 		0], [YELLOW, 	0], [PURPLE, 	1], [BLUE, 		0],	[YELLOW, 	1],
				[YELLOW, 	1], [GREEN, 	0], [GREEN, 	0], [BLUE, 		0],	[BLUE, 		0],
				[YELLOW, 	0], [PURPLE, 	0], [PURPLE, 	1], [RED, 		1],	[RED, 		0]
			]
		},
		//level 6
		{
			target: PURPLE,
			width: 6,
			height: 7,
			board:[
				[GREEN, 	1], [RED, 		0], [BLUE, 		0], [PURPLE, 	1], [BLUE, 		0], [PURPLE, 	0],// [cell's color, steps]
				[GREEN, 	0], [RED, 		0], [BLUE, 		0], [BLUE, 		0],	[BLUE, 		0], [BLUE, 		1],
				[GREEN, 	0], [RED, 		0], [YELLOW, 	1], [YELLOW, 	0],	[YELLOW, 	0], [YELLOW, 	0],
				[RED, 		0], [RED, 		1], [BLUE, 		0], [BLUE, 		0], [RED, 		0], [RED, 		0],
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],	[RED, 		0], [GREEN, 	0],
				[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	1],	[RED, 		1], [GREEN, 	0],
				[PURPLE, 	0], [BLUE, 		0], [PURPLE, 	0], [BLUE, 		0],	[RED, 		0],	[GREEN, 	0]
			]
		},
		//level 7
		{
			target: BLUE,
			width: 8,
			height: 8,
			board:[
				[YELLOW, 	0], [YELLOW, 	0], [BLUE, 		1], [BLUE, 		0], [BLUE, 		1], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0],// [cell's color, steps]
				[YELLOW, 	0], [YELLOW, 	0], [GREEN, 	0], [GREEN, 	0],	[GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[GREEN, 	1], [BLUE, 		0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[GREEN, 	1], [BLUE, 		0], [WHITE, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],
				[GREEN, 	1], [BLUE, 		0], [WHITE, 	0], [YELLOW, 	0],	[PURPLE, 	0], [YELLOW, 	0], [PURPLE, 	1], [YELLOW, 	0],
				[RED, 		0], [BLUE, 		0], [WHITE, 	0], [YELLOW, 	0],	[BLUE, 		0], [YELLOW, 	0], [BLUE, 		0], [YELLOW, 	0],
				[RED, 		0], [BLUE, 		0], [WHITE, 	0], [YELLOW, 	0],	[PURPLE, 	0], [YELLOW, 	0], [PURPLE, 	0], [YELLOW, 	0],
				[RED, 		0], [BLUE, 		0], [WHITE, 	0], [YELLOW, 	0],	[BLUE, 		1], [YELLOW, 	0], [BLUE, 		0], [YELLOW, 	0]
			]
		},
		//level 8
		{
			target: BLUE,
			width: 4,
			height: 6,
			board:[
				[BLUE, 		0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0],// [cell's color, steps]
				[BLUE, 		0], [PURPLE, 	1], [YELLOW, 	0], [YELLOW, 	1],
				[BLUE, 		0], [BLUE, 		1], [YELLOW, 	0], [BLUE, 		1],
				[GREEN, 	0], [GREEN, 	0], [RED, 		1], [GREEN, 	0],
				[PURPLE, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[YELLOW, 	0], [GREEN, 	0], [RED, 		0], [GREEN, 	0]
			]
		},
		//level 9
		{
			target: PURPLE,
			width: 3,
			height: 8,
			board:[
				[RED, 		0], [GREEN, 	0], [YELLOW, 	0],// [cell's color, steps]
				[RED, 		0], [GREEN, 	0], [YELLOW, 	1],
				[YELLOW, 	0], [GREEN, 	0], [RED, 		0],
				[YELLOW, 	0], [PURPLE, 	0], [RED, 		0],
				[YELLOW, 	1], [GREEN, 	0], [RED, 		1],
				[RED, 		1], [GREEN, 	0], [YELLOW, 	0],
				[RED, 		0], [GREEN, 	0], [YELLOW, 	0],
				[RED, 		0], [PURPLE, 	1], [YELLOW, 	0],
			]
		},
		//level 10
		{
			target: RED,
			width: 7,
			height: 7,
			board:[
				[PURPLE, 	1], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [RED, 		1],// [cell's color, steps]
				[BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [GREEN, 	0],	[GREEN, 	0], [BLUE, 		0], [RED, 		0],
				[RED, 		0], [RED, 		1], [GREEN, 	0], [GREEN, 	0],	[GREEN, 	0], [BLUE, 		0], [RED, 		0],
				[YELLOW, 	1], [RED, 		1], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [YELLOW, 	1], [RED, 		0],
				[RED, 		0], [RED, 		0], [GREEN, 	0], [GREEN, 	0],	[GREEN, 	0], [YELLOW, 	0], [RED, 		0],
				[BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [GREEN, 	0],	[GREEN, 	0], [GREEN, 	0], [RED, 		0],
				[GREEN, 	1], [GREEN, 	0], [BLUE, 		0], [BLUE, 		0],	[YELLOW, 	1], [YELLOW, 	0], [RED, 		0]
			]
		},
		//level 11
		{
			target: YELLOW,
			width: 6,
			height: 7,
			board:[
				[GREEN, 	0], [YELLOW, 	0], [RED, 	1], [BLUE, 		1], [PURPLE, 	0], [YELLOW, 	0],// [cell's color, steps]
				[YELLOW, 	0], [YELLOW, 	0], [RED, 	0], [BLUE, 		0],	[PURPLE, 	0], [YELLOW, 	1],
				[RED, 		0], [RED, 		0], [RED, 	0], [BLUE, 		0],	[PURPLE, 	0], [PURPLE, 	0],
				[BLUE, 		0], [BLUE, 		0], [BLUE, 	0], [PURPLE, 	1], [BLUE, 		0], [BLUE, 		0],
				[RED, 		0], [RED, 		0], [RED, 	0], [BLUE, 		0],	[PURPLE, 	0], [PURPLE, 	0],
				[GREEN, 	0], [GREEN, 	0], [RED, 	0], [BLUE, 		0],	[PURPLE, 	0], [GREEN, 	1],
				[YELLOW, 	1], [GREEN, 	0], [RED, 	0], [BLUE, 		1],	[PURPLE, 	1], [GREEN, 	0]
			]
		},
		//level 12
		{
			target: BLUE,
			width: 5,
			height: 5,
			board:[
				[RED, 		0], [PURPLE, 	0], [PURPLE, 	0], [GREEN, 	0], [YELLOW, 	0],// [cell's color, steps]
				[RED, 		0], [RED, 		1], [RED, 		0], [GREEN, 	0], [YELLOW, 	0],
				[BLUE, 		1], [BLUE, 		0], [BLUE, 		0], [GREEN, 	1], [PURPLE, 	1],
				[RED, 		0], [RED, 		1], [RED, 		0], [GREEN, 	0], [YELLOW, 	0],
				[RED, 		0], [PURPLE, 	0], [PURPLE, 	0], [GREEN, 	0], [YELLOW, 	1]
			]
		},
		//level 13
		{
			target: RED,
			width: 8,
			height: 9,
			board:[
				[RED, 		0], [RED, 		0], [GREEN, 	1], [GREEN, 	0], [GREEN, 	0], [YELLOW, 	0], [RED, 		0], [RED, 		0],// [cell's color, steps]
				[RED, 		0], [RED, 		0], [GREEN, 	0], [GREEN, 	0],	[GREEN, 	0], [YELLOW, 	0], [RED, 		1], [RED, 		0],
				[YELLOW, 	0], [YELLOW, 	0], [GREEN, 	0], [GREEN, 	0],	[GREEN, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[YELLOW, 	0], [YELLOW, 	0], [RED, 		0], [RED, 		0],	[RED, 		1], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		1], [BLUE, 		0], [RED, 		0], [RED, 		0],	[RED, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		0], [BLUE, 		0], [RED, 		0], [RED, 		0],	[RED, 		0], [YELLOW, 	1], [YELLOW, 	0], [BLUE, 		1],
				[GREEN, 	0], [GREEN, 	0], [BLUE, 		0], [BLUE, 		0],	[YELLOW, 	0], [YELLOW, 	0], [RED, 		0], [RED, 		0],
				[GREEN, 	0], [GREEN, 	1], [BLUE, 		0], [BLUE, 		0],	[YELLOW, 	0], [YELLOW, 	0], [RED, 		0], [RED, 		0]
			]
		},
		//level 14
		{
			target: GREEN,
			width: 8,
			height: 10,
			board:[
				[BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [YELLOW, 	0], [YELLOW, 	0], [BLUE, 		0], [RED, 		0], [PURPLE, 	0],// [cell's color, steps]
				[BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [YELLOW, 	0], [YELLOW, 	1], [BLUE, 		0], [RED, 		0], [PURPLE, 	0],
				[BLUE, 		0], [BLUE, 		1], [GREEN, 	0], [YELLOW, 	0],	[PURPLE, 	0], [BLUE, 		0], [RED, 		0], [PURPLE, 	1],
				[RED, 		0], [RED, 		0], [RED, 		2], [YELLOW, 	0], [PURPLE, 	0], [GREEN, 	2], [GREEN, 	0], [GREEN, 	0],
				[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [GREEN, 	0],	[GREEN, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],
				[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [GREEN, 	0],	[GREEN, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],
				[RED, 		0], [RED, 		0], [RED, 		2], [YELLOW, 	0], [PURPLE, 	0], [GREEN, 	2], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		0], [BLUE, 		1], [GREEN, 	0], [YELLOW, 	0],	[PURPLE, 	0], [BLUE, 		0], [RED, 		0], [PURPLE, 	0],
				[BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [YELLOW, 	0], [YELLOW, 	1], [BLUE, 		0], [RED, 		0], [PURPLE, 	0],
				[BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [YELLOW, 	0], [YELLOW, 	0], [BLUE, 		0], [RED, 		0], [PURPLE, 	0]
			]
		},
		//level 15
		{
			target: PURPLE,
			width: 12,
			height: 16,
			board:[
				[RED,		0], [RED, 		0], [RED, 		0], [RED, 		0], [RED, 		0], [RED, 		0], [RED, 		0], [RED, 		0], [RED, 		0], [RED, 		1], [RED, 		0], [RED, 		0], // [cell's color, steps]
				[GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [PURPLE, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0],
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],	[PURPLE, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],	[PURPLE, 	0], [PURPLE, 	0], [BLUE, 		0], [BLUE, 		0],
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],	[PURPLE, 	0], [YELLOW, 	0], [YELLOW, 	0], [PURPLE, 	0],	[PURPLE, 	0], [BLUE, 		0], [YELLOW, 	0], [BLUE, 		0],
				[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0],	[PURPLE, 	1], [YELLOW, 	0], [PURPLE, 	0], [PURPLE, 	0],	[BLUE, 		0], [YELLOW, 	0], [YELLOW, 	0], [BLUE, 		0],
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],	[BLUE, 		0], [RED, 		0], [PURPLE, 	0], [BLUE, 		0],	[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [BLUE, 		0],
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],	[BLUE, 		0], [RED, 		0], [PURPLE, 	0], [PURPLE, 	0], [BLUE, 		0], [YELLOW, 	0], [YELLOW, 	0], [BLUE, 		0],
				[RED, 		0], [RED, 		0], [RED, 		0], [RED, 		0],	[BLUE, 		0], [RED, 		0], [RED, 		0], [PURPLE, 	0],	[PURPLE, 	0], [BLUE, 		0], [YELLOW, 	0], [BLUE, 		0],
				[RED, 		0], [RED, 		0], [RED, 		0], [RED, 		0],	[BLUE, 		1], [RED, 		0], [RED, 		0], [RED, 		0], [PURPLE, 	0], [PURPLE, 	0], [BLUE, 		0], [BLUE, 		0],
				[GREEN, 	0], [GREEN, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0],
				[GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	1], [YELLOW, 	0], [BLUE, 		1], [YELLOW, 	0], [PURPLE, 	0], [PURPLE, 	0], [GREEN, 	0], [GREEN, 	0],
				[GREEN, 	0], [GREEN, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [PURPLE, 	0], [PURPLE, 	0], [GREEN, 	0], [GREEN, 	0],
				[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0],	[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0],	[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0],
				[PURPLE, 	0], [RED, 		0], [RED, 		0], [RED, 		0],	[BLUE, 		0], [RED, 		0], [RED, 		0], [RED, 		0],	[GREEN, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0],
				[PURPLE, 	0], [RED, 		0], [GREEN, 	1], [RED, 		0],	[BLUE, 		0], [RED, 		0], [GREEN, 	0], [RED, 		0],	[GREEN, 	0], [BLUE, 		0], [YELLOW, 	1], [GREEN, 	0],
				[PURPLE, 	0], [RED, 		0], [RED, 		0], [RED, 		0],	[BLUE, 		0], [RED, 		0], [GREEN, 	0], [RED, 		0],	[GREEN, 	0], [BLUE, 		0], [YELLOW, 	0], [GREEN, 	0]
			]
		},
		//level 16
		{
			target: PURPLE,
			width: 6,
			height: 6,
			board:[
				[PURPLE, 	0], [RED, 		0], [BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [PURPLE, 	0],// [cell's color, steps]
				[RED, 		0], [RED, 		0], [BLUE, 		0], [BLUE, 		1],	[GREEN, 	0], [GREEN, 	0],
				[YELLOW, 	1], [YELLOW, 	0], [RED, 		0], [RED, 		0],	[YELLOW, 	1], [YELLOW, 	0],
				[YELLOW, 	0], [YELLOW, 	0], [RED, 		0], [RED, 		0], [YELLOW, 	0], [YELLOW, 	0],
				[GREEN, 	0], [GREEN, 	1], [BLUE, 		0], [BLUE, 		0],	[RED, 		0], [RED, 		0],
				[PURPLE, 	1], [GREEN, 	0], [BLUE, 		0], [BLUE, 		1],	[RED, 		0], [PURPLE, 	0]
			]
		},
		//level 17
		{
			target: BLUE,
			width: 5,
			height: 8,
			board:[
				[PURPLE, 	0], [BLUE, 		0], [RED, 		0], [GREEN, 	0], [YELLOW, 	0],// [cell's color, steps]
				[PURPLE, 	0], [BLUE, 		0], [RED, 		0], [GREEN, 	0],	[YELLOW, 	0],
				[PURPLE, 	0], [BLUE, 		0], [RED, 		1], [GREEN, 	0],	[YELLOW, 	1],
				[RED, 		1], [RED, 		1], [GREEN, 	0], [BLUE, 		0], [BLUE, 		0],
				[RED, 		0], [RED, 		0], [GREEN, 	0], [BLUE, 		0],	[BLUE, 		0],
				[PURPLE, 	0], [BLUE, 		2], [RED, 		0], [GREEN, 	0],	[YELLOW, 	1],
				[PURPLE, 	0], [BLUE, 		0], [RED, 		0], [GREEN, 	0],	[YELLOW, 	0],
				[PURPLE, 	0], [BLUE, 		0], [RED, 		0], [GREEN, 	0],	[YELLOW, 	0]
			]
		},
		//level 18
		{
			target: RED,
			width: 9,
			height: 11,
			board:[
				[YELLOW, 	0], [RED, 		1], [GREEN, 	0], [YELLOW, 	0], [PURPLE, 	0], [YELLOW, 	1], [GREEN, 	0], [RED, 		0], [YELLOW, 	1],// [cell's color, steps]
				[RED, 		0], [PURPLE, 	1], [RED, 		0], [GREEN, 	0], [PURPLE, 	0], [GREEN, 	0], [RED, 		1], [PURPLE, 	1], [RED, 		0],
				[GREEN, 	0], [GREEN, 	0], [PURPLE, 	0], [PURPLE, 	0],	[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [GREEN, 	0], [GREEN, 	0],
				[PURPLE, 	0], [GREEN, 	0], [GREEN, 	0], [YELLOW, 	0], [PURPLE, 	0], [YELLOW, 	0], [GREEN, 	0], [GREEN, 	0], [PURPLE, 	0],
				[RED, 		0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0],	[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [RED, 		0],
				[PURPLE, 	0], [GREEN, 	0], [YELLOW, 	0], [GREEN, 	0],	[PURPLE, 	0], [GREEN, 	0], [YELLOW, 	0], [GREEN, 	0], [PURPLE, 	1],
				[GREEN, 	1], [RED, 		0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [RED, 		0], [GREEN, 	1],
				[YELLOW, 	0], [PURPLE, 	1], [GREEN, 	0], [YELLOW, 	0],	[PURPLE, 	0], [YELLOW, 	0], [GREEN, 	0], [PURPLE, 	0], [YELLOW, 	0],
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [PURPLE, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [PURPLE, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [PURPLE, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0]
			]
		},
		//level 19
		{
			target: RED,
			width: 13,
			height: 15,
			board:[
				[BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [WHITE, 	0], [BLUE, 		2], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [GREEN, 	0],// [cell's color, steps]
				[BLUE, 		0], [YELLOW, 	0], [BLUE, 		0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [GREEN, 	0],
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [RED, 		0], [BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		0], [YELLOW, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [RED, 		0], [RED, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [RED, 		1], [BLUE, 		1], [BLUE, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [PURPLE, 	0], [BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [PURPLE, 	0], [PURPLE, 	0], [YELLOW, 	0], [YELLOW, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0], [PURPLE, 	1], [PURPLE, 	0], [PURPLE, 	0], [YELLOW, 	0], [YELLOW, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [YELLOW, 	0], [YELLOW, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		0], [WHITE, 	0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [PURPLE, 	0], [PURPLE, 	0], [YELLOW, 	1], [YELLOW, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [PURPLE, 	0], [BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [RED, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		0], [BLUE, 		0], [WHITE, 	0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [RED, 		0], [BLUE, 		0], [BLUE, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],
				[BLUE, 		0], [BLUE, 		0], [WHITE, 	0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0], [RED, 		0], [RED, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0]
			]
		},
		//level 20
		{
			target: RED,
			width: 4,
			height: 8,
			board:[
				[GREEN, 	1], [RED, 		1], [BLUE, 		0], [GREEN, 	1],// [cell's color, steps]
				[BLUE, 		1], [RED, 		1], [YELLOW, 	1], [PURPLE, 	0],
				[GREEN, 	0], [RED, 		0], [BLUE, 		0], [GREEN, 	0],
				[BLUE, 		0], [RED, 		0], [YELLOW, 	1], [PURPLE, 	0],
				[GREEN, 	0], [RED, 		0], [BLUE, 		0], [GREEN, 	0],
				[BLUE, 		1], [RED, 		0], [YELLOW, 	2], [PURPLE, 	1],
				[GREEN, 	0], [RED, 		0], [BLUE, 		0], [GREEN, 	0],
				[BLUE, 		1], [RED, 		0], [RED, 		0], [RED, 		0]
			]
		},
	],
	stage3: [
		//level 1
		{
			target: BLUE, // level's target
			width:  4, // board width
			height: 5, // board height
			board:[
				[GREEN, 	2], [WHITE, 	0], [RED, 		0], [WHITE, 	0],// [cell's color, steps]
				[WHITE, 	0], [WHITE, 	0], [RED, 		1], [PURPLE, 	0],
				[WHITE, 	0], [YELLOW, 	1], [WHITE, 	0], [WHITE, 	0],
				[WHITE, 	0], [BLUE, 		0], [YELLOW, 	0], [BLUE, 		0],
				[WHITE, 	0], [WHITE, 	0], [BLUE, 		1], [WHITE, 	0]
			]
		},
		//level 2
		{
			target: PURPLE,
			width: 6,
			height: 7,
			board:[
				[WHITE, 	0], [WHITE, 	0], [RED, 		0], [RED, 		1], [WHITE, 	0], [WHITE, 	0],// [cell's color, steps]
				[WHITE, 	0], [WHITE, 	0], [RED, 		0], [GREEN, 	1],	[WHITE, 	0], [WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [YELLOW, 	4],	[WHITE, 	0], [WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0],	[BLUE, 		0], [BLUE, 		0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0], [PURPLE, 	1],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0], [WHITE, 	0]
			]
		},
		//level 3
		{
			target: RED,
			width: 7,
			height: 8,
			board:[
				[PURPLE, 	0], [GREEN, 	0], [GREEN, 	0], [WHITE, 	0], [YELLOW, 	0], [YELLOW, 	0], [BLUE, 		1],// [cell's color, steps]
				[GREEN, 	0], [GREEN, 	0], [WHITE, 	0], [PURPLE, 	2],	[WHITE, 	0], [YELLOW, 	0], [YELLOW, 	0],
				[GREEN, 	0], [WHITE, 	0], [WHITE, 	0], [RED, 		0],	[WHITE, 	0], [WHITE, 	0], [YELLOW, 	0],
				[BLUE, 		0], [WHITE, 	0], [RED, 		0], [RED, 		3], [RED, 		3], [WHITE, 	0], [WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [RED, 		0], [RED, 		0],	[RED, 		1], [WHITE, 	0], [WHITE, 	0],
				[BLUE, 		0], [WHITE, 	0], [WHITE, 	0], [RED, 		3],	[WHITE, 	0], [WHITE, 	0], [PURPLE, 	2],
				[BLUE, 		0], [BLUE, 		0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0], [PURPLE, 	4], [PURPLE, 	1],
				[GREEN, 	0], [BLUE, 		0], [BLUE, 		1], [WHITE, 	0],	[PURPLE, 	0], [PURPLE, 	0], [GREEN, 	1]
			]
		},
		//level 4
		{
			target: YELLOW,
			width: 5,
			height: 8,
			board:[
				[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0],// [cell's color, steps]
				[BLUE, 		0], [BLUE, 		2], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],
				[YELLOW, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],
				[YELLOW, 	4], [YELLOW, 	1], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[YELLOW, 	4], [YELLOW, 	1], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[YELLOW, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],
				[GREEN, 	0], [GREEN, 	2], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],
				[RED, 		0], [RED, 		0], [RED, 		0], [RED, 		0],	[RED, 		0]
			]
		},
		//level 5
		{
			target: PURPLE,
			width: 5,
			height: 8,
			board:[
				[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	4], [PURPLE, 	1],// [cell's color, steps]
				[BLUE, 		0], [BLUE, 		2], [PURPLE, 	0], [WHITE, 	0],	[WHITE, 	0],
				[YELLOW, 	0], [WHITE, 	0], [WHITE, 	0], [PURPLE, 	0],	[WHITE, 	0],
				[YELLOW, 	4], [YELLOW, 	1], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[YELLOW, 	4], [YELLOW, 	1], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[YELLOW, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],
				[GREEN, 	0], [GREEN, 	2], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],
				[RED, 		0], [RED, 		0], [RED, 		0], [RED, 		0],	[RED, 		0]
			]
		},
		//level 6
		{
			target: RED,
			width: 6,
			height: 9,
			board:[
				[GREEN, 	1], [RED, 	0], [WHITE, 	0], [WHITE, 	0], [RED, 		3], [PURPLE, 	2],// [cell's color, steps]
				[YELLOW, 	2], [RED, 	4], [WHITE, 	0], [WHITE, 	0],	[RED, 		4],	[BLUE, 		2],
				[PURPLE, 	0], [RED, 	2], [WHITE, 	0], [WHITE, 	0],	[RED, 		0],	[YELLOW, 	0],
				[BLUE, 		0], [RED, 	3], [WHITE, 	0], [WHITE, 	0], [RED, 		0], [GREEN, 	0],
				[PURPLE, 	0], [RED, 	0], [WHITE, 	0], [WHITE, 	0], [RED, 		0], [YELLOW, 	0],
				[BLUE, 		0], [RED, 	0], [WHITE, 	0], [WHITE, 	0],	[RED, 		0],	[GREEN, 	0],
				[PURPLE, 	0], [RED, 	0], [WHITE, 	0], [WHITE, 	0],	[RED, 		0],	[YELLOW, 	0],
				[BLUE, 		0], [RED, 	0], [WHITE, 	0], [WHITE, 	0],	[RED, 		0],	[GREEN, 	0],
				[PURPLE, 	0], [RED, 	0], [WHITE, 	0], [WHITE, 	0],	[RED, 		3],	[YELLOW, 	0]
			]
		},
		//level 7
		{
			target: BLUE,
			width: 7,
			height: 11,
			board:[
				[RED, 		0], [RED, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [RED, 		0], [RED, 		0],// [cell's color, steps]
				[RED, 		0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],	[WHITE, 	0],	[RED, 		1],
				[YELLOW, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],	[WHITE, 	0],	[YELLOW, 	0],
				[YELLOW, 	0], [WHITE, 	0], [WHITE, 	0], [RED, 		3], [WHITE, 	0], [WHITE, 	0], [YELLOW, 	0],
				[YELLOW, 	0], [WHITE, 	0], [PURPLE, 	0], [BLUE, 		1], [PURPLE, 	1], [WHITE, 	0], [YELLOW, 	0],
				[YELLOW, 	0], [RED, 		0], [BLUE, 		1], [BLUE, 		0],	[BLUE, 		1],	[RED, 		0],	[YELLOW, 	0],
				[YELLOW, 	0], [WHITE, 	0], [PURPLE, 	0], [BLUE, 		1],	[PURPLE, 	0],	[WHITE, 	0],	[YELLOW, 	0],
				[YELLOW, 	0], [WHITE, 	0], [WHITE, 	0], [RED, 		0],	[WHITE, 	0],	[WHITE, 	0],	[YELLOW, 	0],
				[YELLOW, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],	[WHITE, 	0],	[YELLOW, 	0],
				[RED, 		1], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],	[WHITE, 	0],	[RED, 		0],
				[RED, 		0], [RED, 		0], [GREEN, 	0], [GREEN, 	0],	[GREEN, 	1],	[RED, 		0],	[RED, 		0],
			]
		},
		//level 8
		{
			target: YELLOW,
			width: 6,
			height: 8,
			board:[
				[WHITE, 	0], [WHITE, 	0], [RED, 		1], [RED, 		0], [WHITE, 	0], [WHITE, 	0],// [cell's color, steps]
				[WHITE, 	0], [YELLOW, 	1], [GREEN, 	0], [GREEN, 	0],	[YELLOW, 	0],	[WHITE, 	0],
				[RED, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],	[GREEN, 	0],	[RED, 		0],
				[RED, 		0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],	[GREEN, 	0],	[RED, 		0],
				[RED, 		0], [WHITE, 	0], [PURPLE, 	0], [PURPLE, 	0],	[WHITE, 	0],	[RED, 		0],
				[WHITE, 	0], [WHITE, 	0], [PURPLE, 	0], [PURPLE, 	4],	[WHITE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0],	[WHITE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [BLUE, 		0], [RED, 		0], [RED, 		0],	[BLUE, 		1],	[WHITE, 	0]
			]
		},
		//level 9
		{
			target: YELLOW,
			width: 5,
			height: 12,
			board:[
				[RED, 		0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [GREEN, 	0],// [cell's color, steps]
				[RED, 		0], [RED, 		1], [GREEN, 	0], [GREEN, 	1],	[GREEN, 	0],
				[WHITE, 	0], [BLUE, 		0], [YELLOW, 	1], [PURPLE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [BLUE, 		0], [YELLOW, 	0], [PURPLE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [BLUE, 		0], [YELLOW, 	0], [PURPLE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [RED, 		3], [RED, 		0], [RED, 		0],	[WHITE, 	0],
				[WHITE, 	0], [RED, 		0], [RED, 		0], [RED, 		0],	[WHITE, 	0],
				[WHITE, 	0], [BLUE, 		0], [YELLOW, 	0], [PURPLE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [BLUE, 		0], [YELLOW, 	0], [PURPLE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [BLUE, 		1], [YELLOW, 	0], [PURPLE, 	0],	[WHITE, 	0],
				[RED, 		0], [RED, 		1], [GREEN, 	0], [GREEN, 	1],	[GREEN, 	0],
				[RED, 		0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[GREEN, 	0]
			]
		},
		//level 10
		{
			target: BLUE,
			width: 5,
			height: 7,
			board:[
				[WHITE, 	0], [WHITE, 	0], [BLUE, 		3], [WHITE, 	0], [WHITE, 	0],// [cell's color, steps]
				[WHITE, 	0], [RED, 		2], [BLUE, 		0], [WHITE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [YELLOW, 	1], [RED, 		0],	[WHITE, 	0],
				[BLUE, 		0], [BLUE, 		0], [WHITE, 	0], [BLUE, 		0],	[BLUE, 		2],
				[WHITE, 	0], [WHITE, 	0], [YELLOW, 	0], [RED, 		1],	[WHITE, 	0],
				[WHITE, 	0], [RED, 		1], [BLUE, 		0], [WHITE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [BLUE, 		0], [WHITE, 	0],	[WHITE, 	0]
			]
		},
		//level 11
		{
			target: BLUE,
			width: 5,
			height: 7,
			board:[
				[WHITE, 	0], [WHITE, 	0], [BLUE, 		2], [WHITE, 	0], [WHITE, 	0],// [cell's color, steps]
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],
				[YELLOW, 	2], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[PURPLE, 	4],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [GREEN, 	4], [WHITE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0],
				[RED, 		4], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[YELLOW, 	0]
			]
		},
		//level 12
		{
			target: GREEN,
			width: 4,
			height: 6,
			board:[
				[GREEN, 	1], [RED, 		1], [BLUE, 		0], [YELLOW, 	1],// [cell's color, steps]
				[RED, 		0], [RED, 		0], [BLUE, 		0], [BLUE, 		0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[BLUE, 		4], [WHITE, 	0], [WHITE, 	0], [BLUE, 		3],
				[YELLOW, 	1], [RED, 		0], [RED, 		0], [GREEN, 	1]
			]
		},
		//level 13
		{
			target: BLUE,
			width: 5,
			height: 6,
			board:[
				[RED, 		1], [YELLOW, 	4], [WHITE, 	0], [PURPLE, 	0], [RED, 		2],// [cell's color, steps]
				[YELLOW, 	3], [YELLOW, 	0], [WHITE, 	0], [PURPLE, 	0], [PURPLE, 	2],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[GREEN, 	1], [GREEN, 	0], [WHITE, 	0], [BLUE, 		2], [BLUE, 		2],
				[RED, 		0], [GREEN, 	0], [WHITE, 	0], [BLUE, 		0], [RED, 		1]
			]
		},
		//level 14
		{
			target: PURPLE,
			width: 5,
			height: 6,
			board:[
				[YELLOW, 	1], [RED, 		0], [RED, 		1], [WHITE, 	0], [PURPLE, 	2],// [cell's color, steps]
				[BLUE, 		0], [RED, 		0], [WHITE, 	0], [WHITE, 	0], [GREEN, 	3],
				[BLUE, 		0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [GREEN, 	0],
				[BLUE, 		0], [WHITE, 	0], [WHITE, 	0], [RED, 		2], [GREEN, 	0],
				[BLUE, 		0], [RED, 		0], [WHITE, 	0], [WHITE, 	0], [GREEN, 	0],
				[YELLOW, 	0], [RED, 		1], [GREEN, 	0], [WHITE, 	0], [PURPLE, 	1]
			]
		},
		//level 15
		{
			target: BLUE,
			width: 4,
			height: 7,
			board:[
				[WHITE, 	0], [WHITE, 	0], [YELLOW, 	0], [RED, 		2],// [cell's color, steps]
				[RED, 		1], [YELLOW, 	0], [RED, 		0], [BLUE, 		0],
				[YELLOW, 	1], [RED, 		0], [YELLOW, 	0], [RED, 		0],
				[GREEN, 	0], [BLUE, 		0], [WHITE, 	0], [WHITE, 	0],
				[YELLOW, 	0], [GREEN, 	0], [WHITE, 	0], [WHITE, 	0],
				[BLUE, 		0], [YELLOW, 	1], [GREEN, 	4], [WHITE, 	0],
				[WHITE, 	0], [BLUE, 		1], [YELLOW, 	0], [GREEN, 	1]
			]
		},
		//level 16
		{
			target: GREEN,
			width: 6,
			height: 8,
			board:[
				[BLUE, 		0], [BLUE, 		0], [RED, 		1], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0],// [cell's color, steps]
				[PURPLE, 	1], [RED, 		0], [RED, 		0], [RED, 		0],	[PURPLE, 	0],	[PURPLE, 	0],
				[BLUE, 		0], [BLUE, 		0], [YELLOW, 	1], [BLUE, 		0],	[BLUE, 		0],	[BLUE, 		0],
				[BLUE, 		0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],	[BLUE, 		0],	[BLUE, 		0],
				[BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [YELLOW, 	0],	[YELLOW, 	0],	[BLUE, 		1],
				[PURPLE, 	0], [PURPLE, 	0], [PURPLE, 	1], [YELLOW, 	0],	[YELLOW, 	0],	[GREEN, 	1],
				[PURPLE, 	0], [PURPLE, 	0], [GREEN, 	0], [GREEN, 	0],	[YELLOW, 	0],	[YELLOW, 	0],
				[GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0],	[YELLOW, 	0],	[YELLOW, 	0]
			]
		},
		//level 17
		{
			target: PURPLE,
			width: 9,
			height: 15,
			board:[
				[YELLOW, 	0], [YELLOW, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],// [cell's color, steps]
				[YELLOW, 	0], [YELLOW, 	0], [WHITE, 	0], [WHITE, 	0], [GREEN, 	3], [GREEN, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[YELLOW, 	0], [YELLOW, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0], [YELLOW, 	0], [GREEN, 	0], [WHITE, 	0], [WHITE, 	0],
				[YELLOW, 	0], [RED, 		1], [RED, 		0], [RED, 		0], [RED, 		0], [YELLOW, 	0], [YELLOW, 	0], [GREEN, 	0], [WHITE, 	0],
				[RED, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0],	[BLUE, 		0], [BLUE, 		0], [YELLOW, 	0], [YELLOW, 	0], [GREEN, 	0],
				[RED, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0],	[BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [YELLOW, 	0], [BLUE, 		0],
				[RED, 		0], [BLUE, 		0], [PURPLE, 	0], [PURPLE, 	1], [PURPLE, 	0], [PURPLE, 	0], [BLUE, 		0], [YELLOW, 	0], [BLUE, 		0],
				[WHITE, 	0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0],	[BLUE, 		0], [PURPLE, 	1], [PURPLE, 	1], [YELLOW, 	0], [BLUE, 		1],
				[WHITE, 	0], [WHITE, 	0], [RED, 		0], [BLUE, 		0], [BLUE, 		0], [RED, 		0], [PURPLE, 	0], [YELLOW, 	0], [WHITE, 	0],
				[WHITE, 	0], [GREEN, 	3], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[WHITE, 	0], [GREEN, 	0], [GREEN, 	0], [GREEN, 	0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [BLUE, 		0], [WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [PURPLE, 	3], [BLUE, 		0], [BLUE, 		0], [WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],
			]
		},
		//level 18
		{
			target: RED,
			width: 7,
			height: 10,
			board:[
				[BLUE, 		0], [BLUE, 		1], [RED, 		0], [RED, 		0], [RED, 		0], [GREEN, 	0], [PURPLE, 	0],// [cell's color, steps]
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0],	[YELLOW, 	1],	[GREEN, 	0],	[PURPLE, 	0],
				[RED, 		0], [RED, 		0], [RED, 		0], [RED, 		0],	[GREEN, 	0],	[GREEN, 	0],	[PURPLE, 	0],
				[YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [GREEN, 	0],	[GREEN, 	0],	[GREEN, 	0],	[PURPLE, 	0],
				[YELLOW, 	0], [RED, 		0], [YELLOW, 	0], [GREEN, 	0],	[GREEN, 	0],	[GREEN, 	0],	[PURPLE, 	0],
				[BLUE, 		0], [RED, 		0], [YELLOW, 	0], [GREEN, 	0],	[BLUE, 		0],	[PURPLE, 	1],	[PURPLE, 	0],
				[BLUE, 		0], [RED, 		0], [RED, 		0], [GREEN, 	0],	[BLUE, 		0],	[PURPLE, 	1],	[RED, 		0],
				[BLUE, 		0], [RED, 		0], [RED, 		0], [GREEN, 	0],	[BLUE, 		0],	[PURPLE, 	0],	[RED, 		0],
				[BLUE, 		0], [RED, 		0], [GREEN, 	0], [YELLOW, 	1],	[BLUE, 		0],	[PURPLE, 	0],	[RED, 		0],
				[BLUE, 		0], [RED, 		1], [YELLOW, 	0], [YELLOW, 	0],	[BLUE, 		0],	[PURPLE, 	0],	[RED, 		0]
			]
		},
		//level 19
		{
			target: YELLOW,
			width: 6,
			height: 15,
			board:[
				[WHITE, 	0], [BLUE, 		0], [RED, 		1], [GREEN, 	0], [PURPLE, 	0], [YELLOW, 	0],// [cell's color, steps]
				[BLUE, 		0], [RED, 		0], [GREEN, 	0], [PURPLE, 	1],	[BLUE, 		0],	[YELLOW, 	1],
				[BLUE, 		0], [BLUE, 		1], [WHITE, 	0], [GREEN, 	0],	[WHITE, 	0],	[BLUE, 		1],
				[GREEN, 	0], [RED, 		0], [GREEN, 	1], [PURPLE, 	0],	[GREEN, 	0],	[BLUE, 		1],
				[PURPLE, 	1], [RED, 		0], [BLUE, 		0], [GREEN, 	0],	[GREEN, 	1],	[PURPLE, 	0],
				[WHITE, 	0], [RED, 		0], [GREEN, 	0], [PURPLE, 	0],	[WHITE, 	0],	[GREEN, 	0],
				[GREEN, 	0], [GREEN, 	0], [BLUE, 		1], [GREEN, 	0],	[RED, 		1],	[GREEN, 	1],
				[BLUE, 		1], [YELLOW, 	0], [WHITE, 	0], [PURPLE, 	1],	[RED, 		0],	[GREEN, 	0],
				[BLUE, 		0], [YELLOW, 	0], [RED, 		0], [BLUE, 		1],	[PURPLE, 	0],	[WHITE, 	0],
				[WHITE, 	0], [YELLOW, 	1], [GREEN, 	0], [BLUE, 		0],	[GREEN, 	1],	[RED, 		0],
				[PURPLE, 	0], [GREEN, 	0], [WHITE, 	0], [GREEN, 	0],	[RED, 		0],	[WHITE, 	0],
				[RED, 		0], [GREEN, 	0], [GREEN, 	2], [PURPLE, 	1],	[PURPLE, 	0],	[YELLOW, 	0],
				[PURPLE, 	0], [YELLOW, 	0], [BLUE, 		0], [WHITE, 	0],	[PURPLE, 	0],	[YELLOW, 	1],
				[RED, 		1], [WHITE, 	0], [GREEN, 	0], [PURPLE, 	1],	[RED, 		1],	[WHITE, 	0],
				[YELLOW, 	1], [GREEN, 	0], [WHITE, 	0], [GREEN, 	0],	[YELLOW, 	0],	[YELLOW, 	0]
			]
		},
		//level 20
		{
			target: BLUE,
			width: 9,
			height: 12,
			board:[
				[BLUE, 		0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [GREEN, 	0], [GREEN, 	0],// [cell's color, steps]
				[BLUE, 		0], [BLUE, 		1], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [BLUE, 		0], [GREEN, 	0],
				[BLUE, 		0], [WHITE, 	0], [RED, 		0], [WHITE, 	0],	[BLUE, 		2], [WHITE, 	0], [BLUE, 		0], [GREEN, 	0], [GREEN, 	0],
				[WHITE, 	0], [WHITE, 	0], [RED, 		0], [BLUE, 		0], [WHITE, 	0], [WHITE, 	0], [BLUE, 		0], [BLUE, 		0], [GREEN, 	0],
				[WHITE, 	0], [RED, 		0], [RED, 		0], [RED, 		0],	[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [GREEN, 	0], [GREEN, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0],	[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [GREEN, 	0], [GREEN, 	0],
				[PURPLE, 	2], [PURPLE, 	0], [WHITE, 	0], [WHITE, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [BLUE, 		0],
				[PURPLE, 	4], [PURPLE, 	0], [PURPLE, 	0], [WHITE, 	0],	[PURPLE, 	1], [YELLOW, 	0], [YELLOW, 	0], [YELLOW, 	0], [GREEN, 	0],
				[PURPLE, 	0], [PURPLE, 	0], [WHITE, 	0], [PURPLE, 	0], [WHITE, 	0], [YELLOW, 	4], [YELLOW, 	0], [YELLOW, 	0], [GREEN, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [YELLOW, 	3], [YELLOW, 	2], [YELLOW, 	0], [GREEN, 	0], [GREEN, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [GREEN, 	0], [GREEN, 	0],
				[WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [WHITE, 	0], [GREEN, 	0], [GREEN, 	0]
			]
		}
	]
}
