import device;
import AudioManager;
import ui.View as View;
import ui.TextView as TextView;
import src.Config.LevelScreenConfig as config;
import src.lib.uiInflater as uiInflater;
import ui.widget.GridView as GridView;

var BG_WIDTH;
var BG_HEIGHT;

var RED = "#d1536b";
var ORANGE = "#d98239";
var YELLOW = "#EBC658";
//## Class: Application
exports = Class(View, function (supr) {
 this.init = function (opts) {
    opts = merge(opts, {
      x: 0,
      y: 0,
      width: BG_WIDTH,
      height: BG_HEIGHT
    });

    this._sound = new AudioManager({
      path: "resources/sounds/",
      files: {
        click: {
          volume: 1,
          loop: false
        },
        back: {
          volume: 1,
          loop: false
        }
      }
    });

    BG_WIDTH = GLOBAL.screenWidth;
    BG_HEIGHT = GLOBAL.screenHeight;

    app = this;

    supr(this, 'init', [opts]);

    //menu bar
    this.menuBarView = new View(merge({parent: this, x: 0, y: 0}, config.MenuBar));
    uiInflater.addChildren(config.MenuBar.children, this.menuBarView);

    this.menuBarView.backBtn.onInputSelect = function(){
      app._sound.play("back");
      app.emit("LevelScreen:back");
    };

    new TextView({
      parent: this,
      y: 200,
      layout: "box",
      text: "select level",
      color: "#888888",
      horizontalAlign: 'center',
      width: BG_WIDTH,
      height: 100,
      size: 30,
      fontFamily: "Arial"
    });

    this.levelGrid = new GridView({
      parent: this,
      backgroundColor: "white",
      layout: 'box',
      x: 25,
      y: 350,
      width: BG_WIDTH - 50,
      height: 400,
      cols: 5,
      rows: 4,
      autoCellSize: true,
      //Make hide cells out of the grid range...
      hideOutOfRange: true,
      //Make cells in the grid range visible...
      showInRange: true,
      verticalMargin: 10,
      horizontalMargin: 10,
    });

  };

  this.generateLevel = function(stage){

    this.levelGrid.removeAllSubviews();
    this.menuBarView.stage.setText("STAGE " + stage);

    if(stage == 1) this.menuBarView.updateOpts({backgroundColor: RED});
    if(stage == 2) this.menuBarView.updateOpts({backgroundColor: ORANGE});
    if(stage == 3) this.menuBarView.updateOpts({backgroundColor: YELLOW});

    var levelNumber = 1;

    for(i = 0; i < 4; i++){
      for (j = 0; j < 5; j++) {
        var levelView = new View({
          parent: this.levelGrid,
          row: i,
          col: j,
          backgroundColor: RED
        });

        if(stage == 1) levelView.updateOpts({backgroundColor: RED});
        if(stage == 2) levelView.updateOpts({backgroundColor: ORANGE});
        if(stage == 3) levelView.updateOpts({backgroundColor: YELLOW});

        levelView.level = levelNumber;

        new TextView({
          parent: levelView,
          layout: 'box',
          text: levelNumber++,
          color: "white",
          verticalAlign: 'middle',
          horizontalAlign: 'center',
          width: 85,
          height: 80,
          size: 50,
          fontFamily: "Arial"
        });

        if(stage >= GLOBAL.curStage && i*5 + j == GLOBAL.curLv + 1)
        {
          levelView.updateOpts({opacity: 0.5});
        }
        else if(stage >= GLOBAL.curStage && i*5 + j > GLOBAL.curLv + 1)
        {
          levelView.updateOpts({backgroundColor: "#f5f5f5"});
        }

        levelView.onInputSelect = function(){
          app._sound.play("click");
          if(stage <= GLOBAL.curStage || this.level - 1  <= GLOBAL.curLv + 1)
          {
            app.emit('LevelScreen:level', this.level, stage);
          }
        };
      }
    }
  }
});