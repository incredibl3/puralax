import device;
import AudioManager;
import ui.TextView as TextView;
import ui.View as View;
import ui.ImageView as ImageView;
import ui.widget.GridView as GridView;
import ui.widget.ButtonView as ButtonView;

import math.util as util;
import ui.Color as Color;
import src.Config.GameScreenConfig as config;
import src.lib.uiInflater as uiInflater;
import ui.GestureView as GestureView;
import animate;
import src.Level.levels as levels;
import ui.Color as uiColor;

var app;
var BG_WIDTH;
var BG_HEIGHT;
var interval;
var WHITE = levels.color.white;
var RED = levels.color.red;
var GREEN = levels.color.green;
var currentLv;
//## Class: Application
exports = Class(View, function (supr) {
  this.init = function (opts) {
    opts = merge(opts, {
      x: 0,
      y: 0,
      width: BG_WIDTH,
      height: BG_HEIGHT,
      zIndex: 2
    });

    this._sound = new AudioManager({
      path: "resources/sounds/",
      files: {
        move: {
          volume: 1,
          loop: false
        },
        back: {
          volume: 1,
          loop: false
        },
        error: {
          volume: 1,
          loop: false
        },
        paint: {
          volume: 1,
          loop: false
        },
        win: {
          volume: 1,
          loop: false
        },
        loose: {
          volume: 1,
          loop: false
        },
      }
    });

    BG_WIDTH = GLOBAL.screenWidth;
    BG_HEIGHT = GLOBAL.screenHeight;

    app = this;

    supr(this, 'init', [opts]);

    //menu bar
    this.menuBarView = new View(merge({parent: this, x: 0, y: 0}, config.MenuBar));
    uiInflater.addChildren(config.MenuBar.children, this.menuBarView);

    this.menuBarView.backBtn.onInputSelect = function(){
      app._sound.play("back");
      app._gridView.removeFromSuperview();
      app.emit("GameScreen:back", app.stage);
    };

    this.menuBarView.restartBtn.onInputSelect = function(){
      app._sound.play("back");
      app.generateLevel(app.currentLv);
    };

    //win view
    this.winView = new View({
      parent: this,
      x: 0,
      y: 105,
      width: BG_WIDTH,
      height: BG_HEIGHT - 105,
      backgroundColor: "#d1536b",
      zIndex: 3,
      canHandleEvents: false
    });
    this.doneImg = new ImageView({
      parent: this.winView,
      layout: 'box',
      centerX: true,
      centerY: true,
      width: 100,
      height: 100,
      image: "resources/images/done.png",
      canHandleEvents: false
    });

    //Gameover text
    this.goTxt = new TextView({
      parent: this.winView,
      layout: 'box',
      centerX: true,
      centerY: true,
      width: BG_WIDTH,
      height: 200,
      text: "Gameover! Restarting ...",
      color: "white",
      size: 35,
      fontFamily: 'Tahoma',
      zIndex: 1,
      canHandleEvents: false
    });
    this.winView.updateOpts({opacity: 0});

  };

  this.startGame = function(levelNumber, stage){
    this.stage = stage;
    this.currentLv = levelNumber;
    this.generateLevel(levelNumber);
  };

  this.generateLevel = function(level){
    if(this._gridView)
      this._gridView.removeAllSubviews();

    var currentStage;
    if(this.stage == 1)
      this.currentStage = levels.stage1;
    else if(this.stage == 2)
      this.currentStage = levels.stage2;
    else if(this.stage == 3)
      this.currentStage = levels.stage3;

    //get width height of board
    this.boardWidth = this.currentStage[level].width;
    this.boardHeight = this.currentStage[level].height;

    this.cellSize = (BG_WIDTH - 50) / this.boardWidth;

    var bHeight = this.cellSize * this.boardHeight;

    var isCellSizeByWidth = true;
    if(bHeight >= 900){
      this.cellSize = (BG_HEIGHT - 150) / this.boardHeight;
      bHeight = this.cellSize * this.boardHeight;
      isCellSizeByWidth = false;
    }

    //setup board
    this._gridView = new GridView({
      superview: this,
      backgroundColor: "white",
      layout: 'box',
      centerX: true,
      y: 100 + (BG_HEIGHT - 100 - bHeight) / 2,
      width: isCellSizeByWidth ? BG_WIDTH - 50 : this.cellSize * this.boardWidth,
      height: bHeight,
      cols: this.boardWidth,
      rows: this.boardHeight,
      autoCellSize: true,
      //Make hide cells out of the grid range...
      hideOutOfRange: true,
      //Make cells in the grid range visible...
      showInRange: true
    });

    this._gridView.updateOpts({verticalMargin: 10});
    this._gridView.updateOpts({horizontalMargin: 10});

    if(this.boardWidth >= 10 || this.boardHeight >= 10)
    {
      this._gridView.updateOpts({verticalMargin: 3});
      this._gridView.updateOpts({horizontalMargin: 3});
    }else if(this.boardWidth >= 7 || this.boardHeight >= 7)
    {
      this._gridView.updateOpts({verticalMargin: 5});
      this._gridView.updateOpts({horizontalMargin: 5});
    }

    //set game's goal
    this.menuBarView.updateOpts({backgroundColor: this.currentStage[level].target});
    this.menuBarView.stage.setText("STAGE " + this.stage);
    this.menuBarView.level.setText("level " + (level + 1));
    this.winView.updateOpts({backgroundColor: this.currentStage[level].target});

    //game rules
    for (var x = 0; x < this.boardHeight; x++) {
      for (var y = 0; y < this.boardWidth; y++) {
        var square = new GestureView({superview: this._gridView, backgroundColor: this.currentStage[level].board[x * this.boardWidth + y][0], row: x, col: y});
        square.steps = this.currentStage[level].board[x * this.boardWidth + y][1];

        //check this square can move ?
        addStepForSquare(square, this.currentStage[level].board[x * this.boardWidth + y][1]);

        //square on swipe event
        square.on('Swipe', function(angle, dir, numberOfFingers){
          console.log("hungnt on Swipe " + dir + " " + numberOfFingers);
          if(numberOfFingers == 1 && compareColor(this._opts.backgroundColor, "#f5f5f5") == false && this.steps > 0)
          {
            var currentColor = this.style.backgroundColor;
            var targetPos;
            if(dir == "up")
            {
              targetPos = (this._opts.row - 1) * app.boardWidth + this._opts.col;
            }else if(dir == "down"){
              targetPos = (this._opts.row + 1) * app.boardWidth + this._opts.col;
            }else if(dir == "left"){
              targetPos = (this._opts.row) * app.boardWidth + this._opts.col - 1;
            }else if(dir == "right"){
              targetPos = (this._opts.row) * app.boardWidth + this._opts.col + 1;
            }
            var targetSquare = app._gridView.getSubview(targetPos);
            if(compareColor(targetSquare.style.backgroundColor, "#f5f5f5") == true)
            {
              app._sound.play("move");
              this.updateOpts({backgroundColor: WHITE});
              removeAllStep(this);
              targetSquare.updateOpts({backgroundColor: currentColor});
              targetSquare.steps = this.steps - 1;
              addStepForSquare(targetSquare, targetSquare.steps);
              this.steps = 0;
              checkEndGame(level);
            }else{
              if(compareColor(this._opts.backgroundColor, targetSquare._opts.backgroundColor) == false)
              {
                this.steps--;
                remove1step(this);
                spreadColor(this, targetSquare, targetSquare._opts.backgroundColor, this._opts.backgroundColor, level);
              };
            }
            app._gridView.needsRepaint();
            app._gridView.reflow();
          }else{
            app._sound.play("error");
          }
        });
      }
    }
    this._gridView.reflow();
  }

  //spread color
  spreadColor = function(currentSquare, targetSquare, currentColor, targetColor, level)
  {
    var tmpView;
    var targetPos;
    var nextSquare;
    //backgroundColor will change to targetColor after finish animation
    if(targetSquare && compareColor(targetSquare._opts.backgroundColor, currentColor) == true && (targetSquare.isAnimating == false || !targetSquare.isAnimating) )
    {
      app._sound.play("paint");
      tmpView = new GestureView({
        superview: app,
        backgroundColor: targetColor,
        x: targetSquare.style.x + app._gridView.style.x + targetSquare.style.width / 2,
        y: targetSquare.style.y + app._gridView.style.y + targetSquare.style.height / 2,
        width: 0,
        height: 0
      });
      // targetSquare.updateOpts({backgroundColor: currentSquare._opts.backgroundColor});
      targetSquare.isAnimating = true;
      animate(tmpView).now({width: targetSquare.style.width, height: targetSquare.style.height, x: targetSquare.style.x + app._gridView.style.x, y: targetSquare.style.y + app._gridView.style.y}, 150, animate.linear)
      .then(bind(this, function(){
        targetSquare.updateOpts({backgroundColor: currentSquare._opts.backgroundColor});
        targetSquare.isAnimating = false;
        tmpView.removeFromSuperview();
        checkEndGame(level);
      }));
      setTimeout(function(){
         //up square
        if(targetSquare._opts.row > 0)
        {
          targetPos = (targetSquare._opts.row - 1) * app.boardWidth + targetSquare._opts.col;
          nextSquare = app._gridView.getSubview(targetPos);
          spreadColor(targetSquare, nextSquare, currentColor, targetColor, level);
        }

        //down square
        if(targetSquare._opts.row < app.boardHeight - 1)
        {
          targetPos = (targetSquare._opts.row + 1) * app.boardWidth + targetSquare._opts.col;
          nextSquare = app._gridView.getSubview(targetPos);
          spreadColor(targetSquare, nextSquare, currentColor, targetColor, level);
        }

        //left square
        if(targetSquare._opts.col > 0)
        {
          targetPos = (targetSquare._opts.row) * app.boardWidth + targetSquare._opts.col - 1;
          nextSquare = app._gridView.getSubview(targetPos);
          spreadColor(targetSquare, nextSquare, currentColor, targetColor, level);
        }

        //right square
        if(targetSquare._opts.col < app.boardWidth - 1)
        {
          targetPos = (targetSquare._opts.row) * app.boardWidth + targetSquare._opts.col + 1;
          nextSquare = app._gridView.getSubview(targetPos);
          spreadColor(targetSquare, nextSquare, currentColor, targetColor, level);
        }
      }, 70);
    }
  }

  //add white small circle inside color cell
  addStepForSquare = function(square, steps)
  {
    var circleSize = app.cellSize / 8;
    if( steps > 0 )
    {
      for(i = 0; i < steps; i++)
      {
        new ImageView({
          parent: square,
          x: 5 + (circleSize + 5) *i,
          y: 5,
          width: circleSize,
          height: circleSize,
          image: "resources/images/circle.png"
        });
      }
    }
  }

  //remove 1 circle in color cell
  remove1step = function(square){
    if(square.getSubviews().length > 0)
      square.removeSubview(square.getSubview(square.getSubviews().length - 1));
  }

  //remove all circle in color cell
  removeAllStep = function(square){
    if(square.getSubviews().length > 0)
      square.removeAllSubviews();
  }

  checkEndGame = function(level){
    var isWin = true;
    var totalTargetColor = 0;
    var totalStepOnBoard = 0;
    for (var i = 0; i < app.boardWidth * app.boardHeight; i++) {
      var subview = app._gridView.getSubview(i);
      totalStepOnBoard += subview.steps;
      //if board still have color different with target color - > not win yet
      if(subview.isAnimating == true) return;
      if(compareColor(subview.style.backgroundColor, "#f5f5f5") == false && compareColor(subview.style.backgroundColor, app.currentStage[level].target) == false)
      {
        isWin = false;
      }
      if(compareColor(subview.style.backgroundColor, app.currentStage[level].target) == true)
        totalTargetColor++;
    }

    //no more step on board but still have color different with target color -> lose
    //if there is no target color on board -> lose
    if((isWin == false && totalStepOnBoard == 0) || totalTargetColor == 0 )
    {
      app._sound.play("loose");
      console.log("hungnt LOSE !!!!");
      app.goTxt.show();
      app.doneImg.hide();
      animate(app.winView).now({opacity: 1}, 500, animate.linear);
      setTimeout(function(){
        app.winView.updateOpts({opacity: 0});
        app._gridView.removeFromSuperview();
        app.generateLevel(app.currentLv);
      },1500);
      return;
    }

    //show winView when board only have target color
    if(isWin)
    {
      app._sound.play("win");
      app.goTxt.hide();
      app.doneImg.show();
      animate(app.winView).now({opacity: 1}, 500, animate.linear);

      console.log("hungnt puralax_level: " + app.currentLv);
      if(app.currentLv == 19) app.currentLv = -1;

      if (NATIVE.localStorage)
      {
        if(GLOBAL.curLv < app.currentLv || app.currentLv == -1)
          NATIVE.localStorage.setItem("puralax_level", app.currentLv);
      }else{
        if(GLOBAL.curLv < app.currentLv || app.currentLv == -1)
          localStorage.setItem("puralax_level", app.currentLv);
      }
      setTimeout(function(){
        app.winView.updateOpts({opacity: 0});
        app._gridView.removeFromSuperview();

        //increase current level
        if(GLOBAL.curLv == app.currentLv - 1)
          GLOBAL.curLv++;

        app.currentLv++;

        //if end of stage, move to new level of new stage
        if(app.currentLv == 20) {
          app.currentLv = 0;
          GLOBAL.curLv = -1;
          app.stage += 1;

          GLOBAL.curStage++;
          console.log("hungnt puralax_stage: " + GLOBAL.curStage);

          if (NATIVE.localStorage)
          {
            NATIVE.localStorage.setItem("puralax_stage", GLOBAL.curStage);
          }else{
            localStorage.setItem("puralax_stage", GLOBAL.curStage);
          }
        }
        app.generateLevel(app.currentLv);
      },1500);
    }
  }

  compareColor = function(color1, color2)
  {
    var curColor = uiColor.parse(color1);
    var targetColor = uiColor.parse(color2);
    if(curColor.a == targetColor.a && curColor.r == targetColor.r & curColor.g == targetColor.g & curColor.b == targetColor.b)
      return true;
    return false;
  }

  this.endGame = function(){
    app.emit("GameScreen:end");
  }

});