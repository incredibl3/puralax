import device;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;

var RED = "#d1536b";
var ORANGE = "#d98239";
var YELLOW = "#EBC658";

exports = {
  bgWidth: BG_WIDTH,
  bgHeight: BG_HEIGHT,
  startSquareNumber: 2,
  maxSquare: 7,
  colorGap: 15,
  gameTime: 60,
  MenuBar: {
    name: 'MenuBar',
    x: 0,
    y: 0,
    width: BG_WIDTH,
    height: 100,
    direction: "vertical",
    backgroundColor: RED,
    children: [
      {
        cls: "ui.ImageView",
        name: "backBtn",
        x: 20,
        y: 0,
        width: 50,
        height: 100,
        image: "resources/images/back.png",
      },
      {
        cls: "ui.TextView",
        name: "stage",
        text: 'STAGE 1',
        color: 'white',
        x: 90,
        y: 0,
        width: 150,
        height: 100,
        size: 35,
        fontFamily: 'Tahoma',
        zIndex: 1
      }
  ]
  }
};