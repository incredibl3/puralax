import device;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;

var RED = "#d1536b";
var ORANGE = "#d98239";
var YELLOW = "#EBC658";

exports = {
  MenuBar: {
    name: 'MenuBar',
    x: 0,
    y: 0,
    width: BG_WIDTH,
    height: 100,
    direction: "vertical",
    backgroundColor: "#888888",
    children: [
      {
        cls: "ui.TextView",
        layout: "box",
        text: "PURALAX",
        color: "white",
        verticalAlign: 'middle',
        horizontalAlign: 'center',
        width: BG_WIDTH,
        height: 100,
        size: 50,
        fontFamily: "Arial"
      }
    ]
  },
  Stages: {
    name: 'Stages',
    x: 0,
    y: 200,
    width: BG_WIDTH,
    height: 800,
    direction: "vertical",
    zIndex: 2,
    children: [
      {
        cls: "ui.TextView",
        layout: "box",
        text: "select stage",
        color: "#888888",
        horizontalAlign: 'center',
        width: BG_WIDTH,
        height: 100,
        size: 30,
        fontFamily: "Arial"
      },
      {
        cls: "ui.widget.GridView",
        name: "stagesView",
        backgroundColor: "white",
        layout: 'box',
        centerX: true,
        y: 250,
        width: BG_WIDTH - 200,
        height: 100,
        cols: 3,
        rows: 1,
        autoCellSize: true,
        //Make hide cells out of the grid range...
        hideOutOfRange: true,
        //Make cells in the grid range visible...
        showInRange: true,
        verticalMargin: 10,
        horizontalMargin: 20,
        children: [
          {
            cls: "ui.View",
            name: "stage1",
            backgroundColor: RED,
            row: 0,
            col: 0,
            children: [
              {
                cls: "ui.TextView",
                layout: "box",
                text: "1",
                width: 85,
                height: 80,
                color: "white",
                verticalAlign: 'middle',
                horizontalAlign: 'center',
                size: 50,
                fontFamily: "Arial"
              }
            ]
          },
          {
            cls: "ui.View",
            name: "stage2",
            backgroundColor: ORANGE,
            row: 0,
            col: 1,
            children: [
              {
                cls: "ui.TextView",
                layout: "box",
                text: "2",
                color: "white",
                width: 85,
                height: 80,
                verticalAlign: 'middle',
                horizontalAlign: 'center',
                size: 50,
                fontFamily: "Arial"
              }
            ]
          },
          {
            cls: "ui.View",
            name: "stage3",
            backgroundColor: YELLOW,
            row: 0,
            col: 2,
            children: [
              {
                cls: "ui.TextView",
                layout: "box",
                text: "3",
                color: "white",
                width: 85,
                height: 80,
                verticalAlign: 'middle',
                horizontalAlign: 'center',
                size: 50,
                fontFamily: "Arial"
              }
            ]
          }
        ]
      }
    ]
  }
};