import device;

import ui.View as View;
import src.Config.MainMenuScreenConfig as config;
import src.lib.uiInflater as uiInflater;

var app;
var BG_WIDTH;
var BG_HEIGHT;

//## Class: Application
exports = Class(View, function (supr) {
  this.init = function (opts) {
    opts = merge(opts, {
      x: 0,
      y: 0,
      width: BG_WIDTH,
      height: BG_HEIGHT
    });

    BG_WIDTH = GLOBAL.screenWidth;
    BG_HEIGHT = GLOBAL.screenHeight;

    app = this;

    supr(this, 'init', [opts]);

    //menu bar
    this.menuBarView = new View(merge({parent: this, x: 0, y: 0}, config.MenuBar));
    uiInflater.addChildren(config.MenuBar.children, this.menuBarView);

    this.stagesView = new View(merge({parent: this}, config.Stages));
    uiInflater.addChildren(config.Stages.children, this.stagesView);

    this.stagesView.stagesView.stage1.onInputSelect = bind(this, function(){
      this.emit('MainMenuScreen:stage1');
    });

    if(GLOBAL.curStage < 2) this.stagesView.stagesView.stage2.updateOpts({backgroundColor: "#f5f5f5"});
    if(GLOBAL.curStage < 3) this.stagesView.stagesView.stage3.updateOpts({backgroundColor: "#f5f5f5"});

    this.stagesView.stagesView.stage2.onInputSelect = bind(this, function(){
      if(GLOBAL.curStage < 2) return;
      this.emit('MainMenuScreen:stage2');
    });

    this.stagesView.stagesView.stage3.onInputSelect = bind(this, function(){
      if(GLOBAL.curStage < 3) return;
      this.emit('MainMenuScreen:stage3');
    });
  };
});